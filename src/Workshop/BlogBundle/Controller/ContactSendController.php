<?php

namespace Workshop\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Workshop\BlogBundle\Model\Contact;
use Workshop\BlogBundle\Form\ContactType;


class ContactSendController extends Controller
{
    public function indexAction(Request $request)
    {
    	$contact = new contact;
    	$form = $this->createForm(new ContactType, $contact);
    	$form->handleRequest($request);

    	if (!$form->isValid())
        {
    	   $this->get('session')->getFlashBag()->add('error', $form->getErrorsAsString());
        }
    	else
    	{
            $this->get('contact_mail')->send($contact->email,$contact->text);
    	}

    	

		return $this->redirect($this->generateUrl('workshop_blog_homepage'));
    }

}
