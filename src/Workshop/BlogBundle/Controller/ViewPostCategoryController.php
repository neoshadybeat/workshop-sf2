<?php

namespace Workshop\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ViewPostCategoryController extends Controller
{
    public function indexAction($categorySlug)
    {
    	$em = $this->getDoctrine()->getManager();
    	$category = $em->getRepository('WorkshopBlogBundle:Category')->findAllPostsBySlugOrderedBydate($categorySlug);

    	if(is_null($category))
    		throw $this->createNotFoundException('Category not found');
    	
        return $this->render('WorkshopBlogBundle:ViewPostCategoryView:index.html.twig', compact('category'));
    }
}
