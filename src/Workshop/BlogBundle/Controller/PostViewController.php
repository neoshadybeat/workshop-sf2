<?php

namespace Workshop\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PostViewController extends Controller
{
    public function indexAction($slug)
    {
    	$em = $this->getDoctrine()->getManager();
    	$post = $em->getRepository('WorkshopBlogBundle:Post')->findOneBySlug($slug);

    	if(is_null($post))
    		throw $this->createNotFoundException('Post not found');

        return $this->render('WorkshopBlogBundle:PostView:index.html.twig', compact('post'));
    }
}
