<?php

namespace Workshop\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Workshop\BlogBundle\Model\Contact;
use Workshop\BlogBundle\Form\ContactType;

class ContactController extends Controller
{

	private $formFactory, $router;

	public function __construct($formFactory, $router, $templating)
	{
		$this->formFactory = $formFactory;
		$this->router = $router;
		$this->templating = $templating;
	}

	public function indexAction()
	{
	    $contact = new Contact();

	    $form = $this->formFactory->create(new ContactType(), $contact, array(
            'action' => $this->router->generate('workshop_blog_contact_send'),
            'method' => 'POST',
        ));

		return $this->templating->renderResponse('WorkshopBlogBundle:Contact:index.html.twig', array('form' => $form->createView()));
	}
}
