<?php

namespace Workshop\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class WebIndexController extends Controller
{
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$posts = $em->getRepository('WorkshopBlogBundle:Post')->findAllOrderedByDate();
    	
        return $this->render('WorkshopBlogBundle:WebIndex:index.html.twig', compact('posts'));
    }
}
