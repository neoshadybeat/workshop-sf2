<?php

namespace Workshop\BlogBundle\Services;

class ContactMail
{

	private $mailer,$message;

	public function __construct($mailer, $sender)
	{
		$this->message = \Swift_Message::newInstance();
		$this->mailer = $mailer;
		$this->message->setTo($sender)
			   ->setSubject('Contact mail');

	}

	public function send($sendFrom,$body)
	{
		$this->message->setFrom($sendFrom)
					 ->setBody($body)
			         ;

	    $this->mailer->send($this->message);
	}


}